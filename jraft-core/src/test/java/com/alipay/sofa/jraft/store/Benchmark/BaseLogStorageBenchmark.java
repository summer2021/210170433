package com.alipay.sofa.jraft.store.Benchmark;

import com.alipay.sofa.jraft.conf.ConfigurationManager;
import com.alipay.sofa.jraft.entity.codec.LogEntryCodecFactory;
import com.alipay.sofa.jraft.entity.codec.v2.LogEntryV2CodecFactory;
import com.alipay.sofa.jraft.option.LogStorageOptions;
import com.alipay.sofa.jraft.option.StoreOptions;
import com.alipay.sofa.jraft.storage.LogStorage;
import com.alipay.sofa.jraft.store.DefaultLogStorage;
import com.alipay.sofa.jraft.store.file.FileHeader;
import com.alipay.sofa.jraft.store.file.index.IndexFile;
import com.alipay.sofa.jraft.test.TestUtils;
import org.junit.After;
import org.junit.Before;

/**
 * @author hzh (642256541@qq.com)
 */
public abstract class BaseLogStorageBenchmark {


    protected LogStorage logStorage;
    protected String path;
    protected StoreOptions storeOptions;
    private ConfigurationManager confManager;
    private LogEntryCodecFactory logEntryCodecFactory;

    @Before
    public void setup() throws Exception {
        this.path = TestUtils.mkTempDir();
        this.storeOptions = new StoreOptions();
        this.confManager = new ConfigurationManager();
        this.logEntryCodecFactory = LogEntryV2CodecFactory.getInstance();
        this.logStorage = newLogStorage();

        final LogStorageOptions opts = newLogStorageOptions();

        this.logStorage.init(opts);
    }

    protected LogStorage newLogStorage() {

        final StoreOptions storeOptions = new StoreOptions();
        storeOptions.setSegmentFileSize(1024 * 1024 * 256);
        storeOptions.setConfFileSize(1024 * 1024 * 256);
        storeOptions.setIndexFileSize(FileHeader.HEADER_SIZE + 50000 * IndexFile.IndexEntry.INDEX_SIZE);
        return new DefaultLogStorage(this.path, storeOptions);
    }

    protected LogStorageOptions newLogStorageOptions() {
        final LogStorageOptions opts = new LogStorageOptions();
        opts.setConfigurationManager(this.confManager);
        opts.setLogEntryCodecFactory(this.logEntryCodecFactory);
        return opts;
    }

    @After
    public void teardown() throws Exception {
        this.logStorage.shutdown();
    }

}
