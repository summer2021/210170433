package com.alipay.sofa.jraft.store.Benchmark;

import com.alipay.sofa.jraft.entity.LogEntry;
import com.alipay.sofa.jraft.option.RaftOptions;
import com.alipay.sofa.jraft.option.StoreOptions;
import com.alipay.sofa.jraft.storage.LogStorage;
import com.alipay.sofa.jraft.storage.impl.RocksDBLogStorage;
import com.alipay.sofa.jraft.store.BaseLogStorageTest;
import com.alipay.sofa.jraft.store.DefaultLogStorage;
import com.alipay.sofa.jraft.test.TestUtils;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jmh.runner.options.TimeValue;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author hzh (642256541@qq.com)
 */
@State(Scope.Benchmark)
public class LogStorageBenchmark extends BaseLogStorageBenchmark {

    private static final int logSize = 1024 * 16;


    /**
     * test Write
     *
     * default log storage:
     *
     * Benchmark        Mode  Cnt  Score   Error   Units
     * bigBatchSize     thrpt    3  0.025 ± 0.043  ops/ms
     * normalBatchSize  thrpt    3  0.036 ± 0.014  ops/ms
     * smallBatchSize   thrpt    3  0.060 ± 0.044  ops/ms
     *
     * rocksdb log storage:
     * bigBatchSize     thrpt    3  0.022 ± 0.021  ops/ms
     * normalBatchSize  thrpt    3  0.038 ± 0.031  ops/ms
     * smallBatchSize   thrpt    3  0.088 ± 0.220  ops/ms
     */





    @Setup
    public void setup() throws Exception {
        super.setup();
    }

    @TearDown
    public void teardown() throws Exception {
        super.teardown();
    }



    public void testWrite(final int batchSize, final int logSize) {
        final List<LogEntry> entries = new ArrayList<>(batchSize);
        for (int i = 0; i < batchSize; i++) {
            entries.add(TestUtils.mockEntry(i, i, logSize));
        }
        int ret = this.logStorage.appendEntries(entries);
        if (ret != batchSize) {
            System.err.println("Fatal error: write failures, expect " + batchSize + ", but was " + ret);
            System.exit(1);
        }
    }


    @Benchmark
    @BenchmarkMode(Mode.Throughput)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void bigBatchSize() {
        testWrite(500, logSize);
    }


    @Benchmark
    @BenchmarkMode(Mode.Throughput)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void normalBatchSize() {
        testWrite(250, logSize);
    }


    @Benchmark
    @BenchmarkMode(Mode.Throughput)
    @OutputTimeUnit(TimeUnit.MILLISECONDS)
    public void smallBatchSize() {
        testWrite(100, logSize);
    }


    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder() //
                .include(LogStorageBenchmark.class.getSimpleName()) //
                .warmupIterations(1) //
                .warmupTime(TimeValue.seconds(10)) //
                .measurementIterations(3) //
                .measurementTime(TimeValue.seconds(10)) //
                .threads(1) //
                .forks(1) //
                .build();

        new Runner(opt).run();
    }

    @Override
    protected LogStorage newLogStorage() {
        final StoreOptions storeOptions = new StoreOptions();
        storeOptions.setSegmentFileSize(1024 * 1024 * 64);
        //return new RocksDBLogStorage(this.path, new RaftOptions());
        return new DefaultLogStorage(this.path, storeOptions);
    }
}
