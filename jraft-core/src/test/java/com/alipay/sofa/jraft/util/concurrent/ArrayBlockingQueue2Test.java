/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.alipay.sofa.jraft.util.concurrent;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author hzh (642256541@qq.com)
 */
public class ArrayBlockingQueue2Test {

    private BlockingQueue2<Integer> queue = new ArrayBlockingQueue2<>(120);

    @Test
    public void testBlockingDrainTo() throws InterruptedException {
        int totalNum = 100;
        int cnt = 0;

        for (int i = 0; i < 2; i++) {
            final Thread thread = new Thread(() -> {
                for (int j = 0; j < totalNum / 2; j++) {
                    try {
                        queue.offer(j, 100, TimeUnit.MILLISECONDS);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            thread.start();
        }
        final List<Integer> result = new ArrayList<>();
        int num;
        while (true) {
            while ((num = queue.blockingDrainTo(result, totalNum, 100, TimeUnit.MILLISECONDS)) == 0);
            Assert.assertEquals(num, result.size());
            System.out.println("size : " + num);
            result.clear();
            cnt += num;
            if (cnt == totalNum) {
                break;
            }
        }
    }
}